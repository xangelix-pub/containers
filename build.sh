build() {
  local platform=$1

  local img=$2

  local stag=$3
  if [ -z $5 ]; then
    local ltag="latest"
  else
    local ltag=$5
  fi

  local repo=$4

  local tag1=$repo/$img:$stag
  local tag2=$repo/$img:$ltag

  $platform build -t $tag1 -t $tag2 --layers=true --squash -f $(dirname "$0")/$img/Containerfile $(dirname "$0")/$img/
  $platform push $tag1
  $platform push $tag2
}

build $1 $2 $3 $4
